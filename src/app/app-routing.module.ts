import { NgModule, Injectable, Type } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ContainerComponent } from './container/container.component';
 const routes: Routes = [
     {
         path: '',
         component: ContainerComponent,
         children: [
             {
                path: 'projects',
                loadChildren: () => import('./projects/projects.module').then(m => m.ProjectsModule)
             }
         ]
     }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  providers: [
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
