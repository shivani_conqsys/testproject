import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [],
  imports: [
    FormsModule,
    NgxChartsModule
  ],
  exports: [ 
    FormsModule,
    NgxChartsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule  { }
