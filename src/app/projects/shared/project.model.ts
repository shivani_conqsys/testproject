export class ProjectModel {
    id: number = 0;
    projectName: string;
    Value: string;
    startingDate: any;
    Actions: any;
    customerName: string;
    projectNumber: number;
    projectValue: number;
    projectStage: string;
    notes: string;
}
