import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OnInit, Component, Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


@Injectable({
    providedIn: 'root'
})

export class ProjectService {
    constructor(private httpClient: HttpClient) {
    }

    getProjectDetails() {
        const url = '../../../assets/json/project.json';
        return this.httpClient.get(url).map((response: any) =>
        {
            return response
        });
    }

}