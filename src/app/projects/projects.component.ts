import { Component, OnInit } from '@angular/core';
import { ProjectService } from './shared/project.service';
import { Router } from '@angular/router';
import { ProjectModel } from './shared/project.model';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})

export class ProjectsComponent implements OnInit {
  displayedColumns: string[] = ['projectName', 'Value', 'startingDate', 'Actions'];
  dataSource: ProjectModel[] = [];

  constructor(
    private projectService: ProjectService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.getProjectDetails();
  }

  getProjectDetails() {
    this.projectService.getProjectDetails().subscribe((res: any) => {
      if (res) {
        this.dataSource = res.projectList;
      }
    });
  }

  addNewProject() {
    this.router.navigate(['projects/new']);
  }

  projectDetail() {
    this.router.navigate(['projects/detail']);
  }

}
