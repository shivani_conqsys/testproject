import { NgModule, Injectable, Type } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './projects.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { NewProjectComponent } from './new-project/new-project.component';
import { PrDetailsDialogueComponent } from './project-details/pr/pr-details/pr-details-diagolue.component';
import { OfferDetailsComponent } from './project-details/offers/offer-details/offer-details.component';
import { ProjectUserDetailsComponent } from './project-details/project-users/project-user-details/project-user-details.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectsComponent
  },
  {
    path: 'detail',
    component: ProjectDetailsComponent,
  },
  {
    path: 'pr',
    component: PrDetailsDialogueComponent
  },
  {
    path: 'offer-detail',
    component: OfferDetailsComponent
  },
  {
    path: 'new',
    component: NewProjectComponent
  },
  {
    path: 'project-user',
    component: ProjectUserDetailsComponent
  },
  { path: '**', redirectTo: '/projects' }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [
  ],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
