import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects.component';
import { ProjectRoutingModule } from './projects-routing.module';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { NewProjectComponent } from './new-project/new-project.component';
import { ProjectWarehousesComponent } from './project-details/project-warehouses/project-warehouses-list.component';
import { AngularMaterialModule } from './shared/material.module';
import { SharedModule } from './shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { WarehouseModalComponent } from './project-details/shared-components/warehouse-modal/warehouse-modal.component';
import { MapComponent } from './project-details/shared-components/map/map.component';
import { OfferDetailsComponent } from './project-details/offers/offer-details/offer-details.component';
import { PrDetailsDialogueComponent } from './project-details/pr/pr-details/pr-details-diagolue.component';
import { OfferListComponent } from './project-details/offers/offer-list.component';
import { OverviewListComponent } from './project-details/overview/overview-list.component';
import { PrListComponent } from './project-details/pr/PrListComponent';
import { ProjectUserListComponent } from './project-details/project-users/project-user-list.component';
import { OrderListComponent } from './project-details/orders/order-list.component';
import { ItemsComponent } from './project-details/offers/offer-details/items/items.component';
import { BrandsComponent } from './project-details/offers/offer-details/brands/brands.component';
import { OrderDetailsComponent } from './project-details/orders/order-details/order-details.component';
import { ProjectUserDetailsComponent } from './project-details/project-users/project-user-details/project-user-details.component';

@NgModule({
  declarations: [
    ProjectsComponent, ProjectDetailsComponent, OverviewListComponent,PrListComponent ,
    OfferListComponent,ProjectUserListComponent , ProjectWarehousesComponent,
    NewProjectComponent, MapComponent, WarehouseModalComponent, OfferDetailsComponent,
    PrDetailsDialogueComponent , OrderListComponent,ItemsComponent,BrandsComponent,
    OrderDetailsComponent,ProjectUserDetailsComponent
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    AngularMaterialModule,
    SharedModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: ''
    })
  ],
  exports: [],
  entryComponents: [WarehouseModalComponent, PrDetailsDialogueComponent, OrderDetailsComponent]
})
export class ProjectsModule { }
