import { NgModule, Injectable, Type } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectDetailsComponent } from '../project-details/project-details.component';
import { PrDetailsDialogueComponent } from '../project-details/pr/pr-details/pr-details-diagolue.component';
import { ProjectsComponent } from '../projects.component';
import { NewProjectComponent } from '../new-project/new-project.component';

const routes: Routes = [
  { path: '', redirectTo: 'projects', pathMatch: 'full' },
  {
    path: 'projects',
    component: ProjectsComponent
  },
  {
    path: 'detail',
    component: ProjectDetailsComponent,
  },
  {
    path: 'pr',
    component: PrDetailsDialogueComponent
  },
  {
    path: 'new',
    component: NewProjectComponent
  },
  { path: '**', redirectTo: '/projects' }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [
  ],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
