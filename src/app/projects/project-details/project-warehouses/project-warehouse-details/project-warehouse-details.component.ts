import { Component, OnInit, Inject } from '@angular/core';
import { ProjectWareHouseModel } from '../shared/project-warehouse.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProjectWarehouseService } from '../shared/project-warehouse.service';
@Component({
    selector: 'app-project-warehouse-details',
    templateUrl: './project-warehouse-details.component.html',
    styleUrls: ['./project-warehouse-details.component.css']
})
export class ProjectWarehouseDetailsComponent implements OnInit {
    projectWareHouseModel: ProjectWareHouseModel = new ProjectWareHouseModel();
    isFilledAllRequiredField: boolean;
    animal: string;
    name: string;
    constructor(
        private projectWarehouseService: ProjectWarehouseService) {
    }

    ngOnInit(): void {
        console.log('project warehouse detail called')
        this.isFilledAllRequiredField = true;
    }
}

