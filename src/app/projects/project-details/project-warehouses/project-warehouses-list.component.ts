import { Component, OnInit, ViewChild } from '@angular/core';
import { ProjectWareHouseModel } from './shared/project-warehouse.model';
import { SelectionModel } from '@angular/cdk/collections';
import { ProjectWarehouseService } from './shared/project-warehouse.service';
import { MatDialog } from '@angular/material/dialog';
import { WarehouseModalComponent } from '../shared-components/warehouse-modal/warehouse-modal.component';


@Component({
  selector: 'app-project-warehouses-list',
  templateUrl: './project-warehouses-list.component.html',
  styleUrls: ['./project-warehouses-list.component.css']
})
export class ProjectWarehousesComponent implements OnInit {
  displayedColumns: string[] = ['select', 'name', 'details', 'location', 'status', 'Actions'];
  dataSource: ProjectWareHouseModel[] = [];
  selection = new SelectionModel<ProjectWareHouseModel>(true, []);
  animal: string;
  name: string;
  constructor(private projectWarehouseService: ProjectWarehouseService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getProjectWarehouseDetails();
  }

  addNewWarehouse() {
    this.openDialog();
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(WarehouseModalComponent, {
      width: '1000px',
      height: '800px',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  getProjectWarehouseDetails() {
    this.projectWarehouseService.getProjectWarehouseDetails().subscribe((res: any) => {
      if (res) {
        this.dataSource = res.warehouseList;
      }
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ProjectWareHouseModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  viewWarehouseDetails() {
    this.openDialog();
  }

}
