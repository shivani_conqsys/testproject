import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectWarehousesComponent } from './project-warehouses-list.component';

describe('ProjectWarehousesComponent', () => {
  let component: ProjectWarehousesComponent;
  let fixture: ComponentFixture<ProjectWarehousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectWarehousesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectWarehousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
