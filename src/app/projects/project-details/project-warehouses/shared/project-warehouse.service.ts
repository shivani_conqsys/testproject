import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
})

export class ProjectWarehouseService {
    constructor(private httpClient: HttpClient) {
    }

    getProjectWarehouseDetails() {
        const url = '../../../assets/json/warehouse.json';
        return this.httpClient.get(url).map((response: any) => {
            return response
        });
    }

}