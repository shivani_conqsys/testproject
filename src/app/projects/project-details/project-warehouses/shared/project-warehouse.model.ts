export class ProjectWareHouseModel {
    id: number;
    name: string;
    details: string;
    location: string;
    status: string;
    Actions: any;
    position: number;
    region: string;
    city: string;
    street: string;
}