import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from './shared/dialog-data.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProjectWareHouseModel } from '../../project-warehouses/shared/project-warehouse.model';
@Component({
  selector: 'warehouse-modal',
  templateUrl: 'warehouse-modal.component.html',
  styleUrls: ['./warehouse-modal.component.scss']
})
export class WarehouseModalComponent {
  projectWareHouseModel: ProjectWareHouseModel = new ProjectWareHouseModel();
  contactForm: FormGroup;
  isShow = false;
  constructor(
    public dialogRef: MatDialogRef<WarehouseModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder) {
    this.contactForm = fb.group({
      firstName: '',
      lastName: '',
      email: ''
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onClickPanel(){
    this.isShow = true;
  }

}