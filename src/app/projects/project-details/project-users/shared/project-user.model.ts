export class ProjectUserModel {
    userDetails: string;
    userPosition: string;
    level: string;
    status: string;
    position: number;
}