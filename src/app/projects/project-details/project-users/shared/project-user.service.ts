import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
})

export class ProjectUserService {
    constructor(private httpClient: HttpClient) {
    }

    getProjectUserDetails() {
        const url = '../../../assets/json/project-user.json';
        return this.httpClient.get(url).map((response: any) =>
        {
            return response
        });
    }

}