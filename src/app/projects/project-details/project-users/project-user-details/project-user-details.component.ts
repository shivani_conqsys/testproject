import { Component, OnInit, OnChanges } from '@angular/core';
import { ProjectUserModel } from '../shared/project-user.model';

@Component({
  selector: 'app-project-user-details',
  templateUrl: './project-user-details.component.html',
  styleUrls: ['./project-user-details.component.css']
})
export class ProjectUserDetailsComponent implements OnInit , OnChanges {
    projectUserModel: ProjectUserModel = new ProjectUserModel();
    isFilledAllRequiredField: boolean;
constructor() {
}

ngOnInit() {
    this.isFilledAllRequiredField = true;
}


ngOnChanges() {
    this.isFilledAllRequiredField = true;
  }


  validate(formStatus) {
    if (!formStatus) {
      this.isFilledAllRequiredField = false;
      return;
    }
    this.onSaveProjectUser();
  }

  onSaveProjectUser() {

  }

}