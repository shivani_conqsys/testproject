import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { ProjectUserModel } from './shared/project-user.model';
import { ProjectUserService } from './shared/project-user.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
@Component({
  selector: 'app-project-list-users',
  templateUrl: './project-user-list.component.html',
  styleUrls: ['./project-user-list.component.css']
})
export class ProjectUserListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'userDetails', 'userPosition', 'level','status', 'action'];
  dataSource;
  selection = new SelectionModel<ProjectUserModel>(true, []);
  constructor(private projectUserService: ProjectUserService,
    private router: Router) { }

  ngOnInit(): void {
    this.getProjectUserDetails();
  }

  getProjectUserDetails() {
    this.projectUserService.getProjectUserDetails().subscribe((res: any) => {
      if (res) {
        this.dataSource = new MatTableDataSource<ProjectUserModel>(res.projectUserList);
      }
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    let numRows;
    const numSelected = this.selection.selected.length;
    if(this.dataSource && this.dataSource.length) {
       numRows = this.dataSource.length;
    }
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ProjectUserModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  viewProjectUser() {
    this.router.navigate(['projects/project-user']);
  }



}
