import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { PRModel } from './shared/pr.model';
import { PRService } from './shared/pr.service';
import { PrDetailsDialogueComponent } from './pr-details/pr-details-diagolue.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pr-list',
  templateUrl: './pr-list.component.html',
  styleUrls: ['./pr-list.component.css']
})
export class PrListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'date', 'type', 'number', 'action'];
  dataSource;
  selection = new SelectionModel<PRModel>(true, []);
  prModel: PRModel = new PRModel();
  constructor(private prService: PRService,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getPRDetails();
  }

  getPRDetails() {
    this.prService.getPRList().subscribe((res: any) => {
      if (res) {
        this.dataSource = new MatTableDataSource<PRModel>(res.prList);
      }
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    let numRows;
    const numSelected = this.selection.selected.length;
    if(this.dataSource && this.dataSource.length) {
       numRows = this.dataSource.length;
    }
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PRModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PrDetailsDialogueComponent, {
      width: '1000px',
      height: '800px',
     data : this.prModel
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.prModel = result;
    });
  }

  viewPRDetails() {
    this.router.navigate(['projects/pr']);
  }

  
}
