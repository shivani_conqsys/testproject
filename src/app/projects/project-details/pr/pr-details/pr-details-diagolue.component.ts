import { Component, OnInit, ViewChild } from '@angular/core';
import { PRService } from '../shared/pr.service';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Group, PRDetailModel } from './shared/group.model';
@Component({
  selector: 'app-pr-details-diagolue',
  templateUrl: './pr-details-diagolue.component.html',
  styleUrls: ['./pr-details-diagolue.component.css']
})
export class PrDetailsDialogueComponent implements OnInit {
  vendorList = ['Amazon', 'Flipkart'];
  public dataSource = new MatTableDataSource<any | Group>([]);
  columns: any[];
  displayedColumns: string[];
  groupByColumns: string[] = [];
  allData: any[];
  _allGroup: any[];
  selection = new SelectionModel<PRDetailModel>(true, []);
  expandedCar: any[] = [];
  expandedSubCar: any[] = [];
  @ViewChild(MatSort) sort: MatSort;
  constructor(private prService: PRService,
    private router: Router) {
    this.columns = [
      { field: 'select' },
      {
        field: 'Product'
      }, {
        field: 'Qty'
      }, {
        field: 'Vendor'
      }, {
        field: 'Contract'
      },
      {
        field: 'Price'
      }, {
        field: 'category'
      }
    ];
    this.displayedColumns = this.columns.map(column => column.field);
    this.groupByColumns = ['category'];
  }

  ngOnInit(): void {
    this.getPRDetails();
  }

  getPRDetails() {
    this.prService.getPRDetails().subscribe((res: any) => {
      if (res) {
        this.allData = res.prDetail;
        this.dataSource.data = this.getGroups(this.allData, this.groupByColumns);
      }
    });
  }

  addProductFromCatalog() {
  }


  sendRFQ() {
  }

  issuePO() {

  }

  addProduct() {

  }

  back() {
    this.router.navigate(['projects/detail']);
  }

  groupHeaderClick(row) {
    if (row.expanded) {
      row.expanded = false;
      this.dataSource.data = this.getGroups(this.allData, this.groupByColumns);
    } else {
      row.expanded = true;
      this.expandedCar = row;
      this.dataSource.data = this.addGroupsNew(this._allGroup, this.allData, this.groupByColumns, row);
    }
  }

  getGroups(data: any[], groupByColumns: string[]): any[] {
    const rootGroup = new Group();
    rootGroup.expanded = false;
    return this.getGroupList(data, 0, groupByColumns, rootGroup);
  }

  getGroupList(data: any[], level: number = 0, groupByColumns: string[], parent: Group): any[] {
    if (level >= groupByColumns.length) {
      return data;
    }
    let groups = this.uniqueBy(
      data.map(
        row => {
          const result = new Group();
          result.level = level + 1;
          for (let i = 0; i <= level; i++) {
            result[groupByColumns[i]] = row[groupByColumns[i]];
          }
          return result;
        }
      ),
      JSON.stringify);

    const currentColumn = groupByColumns[level];
    let subGroups = [];
    groups.forEach(group => {
      const rowsInGroup = data.filter(row => group[currentColumn] === row[currentColumn]);
      group.totalCounts = rowsInGroup.length;
      this.expandedSubCar = [];
    });
    groups = groups.sort((a: any, b: any) => {
      const isAsc = 'asc';
      return this.compare(a.category, b.category, isAsc);

    });
    this._allGroup = groups;
    return groups;
  }

  addGroupsNew(allGroup: any[], data: any[], groupByColumns: string[], dataRow: any): any[] {
    const rootGroup = new Group();
    rootGroup.expanded = true;
    return this.getSublevelNew(allGroup, data, 0, groupByColumns, rootGroup, dataRow);
  }



  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    let numRows;
    const numSelected = this.selection.selected.length;
    if (this.dataSource && this.dataSource.data.length) {
      numRows = this.dataSource.data.length;
    }
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }


  getSublevelNew(allGroup: any[], data: any[], level: number, groupByColumns: string[], parent: Group, dataRow: any): any[] {
    if (level >= groupByColumns.length) {
      return data;
    }
    const currentColumn = groupByColumns[level];
    let subGroups = [];
    allGroup.forEach(group => {
      const rowsInGroup = data.filter(row => group[currentColumn] === row[currentColumn]);
      group.totalCounts = rowsInGroup.length;

      if (group.category == dataRow.category.toString()) {
        group.expanded = dataRow.expanded;
        const subGroup = this.getSublevelNew(allGroup, rowsInGroup, level + 1, groupByColumns, group, dataRow.category.toString());
        this.expandedSubCar = subGroup;
        subGroup.unshift(group);
        subGroups = subGroups.concat(subGroup);
      } else {
        subGroups = subGroups.concat(group);
      }
    });
    return subGroups;
  }

  uniqueBy(a, key) {
    const seen = {};
    return a.filter((item) => {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  isGroup(index, item): boolean {
    return item.level;
  }

  onSortData(sort: MatSort) {
    let data = this.allData;
    const index = data.findIndex(x => x['level'] == 1);
    if (sort.active && sort.direction !== '') {
      if (index > -1) {
        data.splice(index, 1);
      }

      data = data.sort((a: any, b: any) => {
        const isAsc = sort.direction === 'asc';
        switch (sort.active) {
          case 'productName':
            return this.compare(a.productName, b.productName, isAsc);
          case 'qty':
            return this.compare(a.qty, b.qty, isAsc);
          case 'category':
            return this.compare(a.category, b.category, isAsc);
          case 'vendor':
            return this.compare(a.vendor, b.vendor, isAsc);
          case 'availability':
            return this.compare(a.availability, b.availability, isAsc);
          case 'price':
            return this.compare(a.price, b.price, isAsc);
          default:
            return 0;
        }
      });
    }
    this.dataSource.data = this.addGroupsNew(this._allGroup, data, this.groupByColumns, this.expandedCar);
  }

  private compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


}