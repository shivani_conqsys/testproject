import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
})

export class PRService {
    constructor(private httpClient: HttpClient) {
    }

    getPRList() {
        const url = '../../../assets/json/pr.json';
        return this.httpClient.get(url).map((response: any) =>
        {
            return response
        });
    }


    getPRDetails() {
        const url = '../../../assets/json/pr-detail.json';
        return this.httpClient.get(url).map((response: any) =>
        {
            return response
        });
    }

}