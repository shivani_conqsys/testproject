export class OrderModel {
    date: any = '';
    type: string = '';
    number: number = 0;
    action: any = '';
    position: number = 0;
    total: string = '';
    status: string = '';
}