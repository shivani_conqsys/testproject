import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
})

export class OrderService {
    constructor(private httpClient: HttpClient) {
    }

    getOrderDetails() {
        const url = '../../../assets/json/order.json';
        return this.httpClient.get(url).map((response: any) =>
        {
            return response
        });
    }

}