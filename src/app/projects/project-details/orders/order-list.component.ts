import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { OrderModel } from './shared/order.model';
import { OrderService } from './shared/order.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  orderModel: OrderModel = new OrderModel();
  displayedColumns: string[] = ['select', 'date', 'type', 'number','total','status', 'action'];
  dataSource;
  selection = new SelectionModel<OrderModel>(true, []);
  constructor(private orderService: OrderService,
    public dialog: MatDialog,) { }

  ngOnInit(): void {
    this.getOrderDetails();
  }

  getOrderDetails() {
    this.orderService.getOrderDetails().subscribe((res: any) => {
      if (res) {
        this.dataSource = new MatTableDataSource<OrderModel>(res.orderList);
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(OrderDetailsComponent, {
      width: '70%',
      // height: '100vh',
      data: this.orderModel
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.orderModel = result;
    });
  }

      /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    let numRows;
    const numSelected = this.selection.selected.length;
    if(this.dataSource && this.dataSource.length) {
       numRows = this.dataSource.length;
    }
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: OrderModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  viewOrderDetails() {
    this.openDialog();
  }

}
