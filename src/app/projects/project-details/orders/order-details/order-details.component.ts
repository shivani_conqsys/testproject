import { Component, OnInit, Inject } from '@angular/core';
import { OrderModel } from '../shared/order.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  orderModel: OrderModel = new OrderModel();
    constructor( public dialogRef: MatDialogRef<OrderDetailsComponent>,
      @Inject(MAT_DIALOG_DATA) public data: OrderModel,) {
    }

    ngOnInit() {
    }

    onNoClick(): void {
      this.dialogRef.close();
    }
  
}
