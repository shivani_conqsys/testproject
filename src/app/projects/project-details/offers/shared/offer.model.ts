export class OfferModel {
    date: any;
    type: string;
    number: number;
    total: string;
    action: any;
    status: string;
    position: number;
}