import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
})

export class OfferService {
    constructor(private httpClient: HttpClient) {
    }

    getOfferDetails() {
        const url = '../../../assets/json/offers.json';
        return this.httpClient.get(url).map((response: any) =>
        {
            return response
        });
    }

}