import { Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { OfferModel } from './shared/offer.model';
import { OfferService } from './shared/offer.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.css']
})
export class OfferListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'date', 'type', 'number', 'total', 'status', 'action'];
  dataSource;
  selection = new SelectionModel<OfferModel>(true, []);
  constructor(
    private offerService: OfferService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.getOfferDetails();
  }

  getOfferDetails() {
    this.offerService.getOfferDetails().subscribe((res: any) => {
      if (res) {
        this.dataSource = new MatTableDataSource<OfferModel>(res.offerList);
      }
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    let numRows;
    const numSelected = this.selection.selected.length;
    if (this.dataSource && this.dataSource.length) {
      numRows = this.dataSource.length;
    }
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: OfferModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onClickOffer() {
    this.router.navigate(['projects/offer-detail']);
  }


}
