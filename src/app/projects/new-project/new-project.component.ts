import { Component, OnInit, OnChanges } from '@angular/core';
import { ProjectModel } from '../shared/project.model';
import { Router } from '@angular/router';

@Component({
  selector: 'new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})

export class NewProjectComponent implements OnInit, OnChanges {
  projectModel: ProjectModel = new ProjectModel();
  isFilledAllRequiredField: boolean;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.isFilledAllRequiredField = true;
  }

  ngOnChanges() {
    this.isFilledAllRequiredField = true;
  }

  validate(formStatus) {
    if (!formStatus) {
      this.isFilledAllRequiredField = false;
      return;
    }
    this.onSaveEmployee();
  }


  onSaveEmployee() {
    console.log('Project Model JSON Data', this.projectModel);
    // if (this.projectModel.id) {
    //   const projectList = JSON.parse(localStorage.getItem('list'));
    //   projectList.filter(emp => {
    //     if (emp.id === this.projectModel.id) {
    //       Object.assign(emp, this.projectModel)
    //     }
    //   });
    //   this.storeToLocalStorage(projectList)
    // } else {
    //   const projectList = JSON.parse(localStorage.getItem('list'));
    //   this.projectModel.id = projectList.length + 1
    //   projectList.push(this.projectModel);
    //   this.storeToLocalStorage(projectList)
    // }
    this. back();
  }

  storeToLocalStorage(projectList?) {
    localStorage.removeItem('list')
    localStorage.setItem('list', JSON.stringify(projectList));
    this.back();
  }

  back() {
    this.router.navigate(['projects']);
  }

}