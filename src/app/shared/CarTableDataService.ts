import { HttpClient } from '@angular/common/http';
import { OnInit, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CarTableDataService implements OnInit {
  constructor(private http: HttpClient) {
  }
  ngOnInit() {
  }
  getAllData(): Observable<any[]> {
    return this.http.get<any[]>('./assets/data/cars-large.json');
  }
}
